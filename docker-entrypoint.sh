#!/bin/bash

./wait-for-it.sh "database:5432"

alembic upgrade head || exit 1

uvicorn "app.main:app" --host 0.0.0.0 --port 8000 || exit 1
