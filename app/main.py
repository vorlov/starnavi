from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

load_dotenv()

from v1 import (init_v1_router,
                auth as v1_auth,
                endpoints as v1_endpoints,
                dependencies as v1_dependencies)
from .openapi import APP_SETTINGS
from .container import Container


def get_application():
    container = Container()

    container.config.gateways.postgres.url.from_env('DATABASE_URI')

    container.wire(modules=[v1_endpoints.signup,
                            v1_endpoints.post,
                            v1_endpoints.user,
                            v1_auth,
                            v1_dependencies])

    _app = FastAPI(**APP_SETTINGS)
    init_v1_router(_app)

    _app.add_middleware(
        CORSMiddleware,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    return _app


app = get_application()
