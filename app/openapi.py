APP_SETTINGS = {
    'title': 'Postuman API',
    'contact': {
        'name': 'Volodya Orlov',
        'email': 'volodka.orlov@gmail.com'
    },
    'version': '0.0.1',
    'description': """
    Postuman API allows you to manipulate a single social network with articles. 💫
    
    You can:
        * register users
        * create articles
        * like and dislike it
        * get some statistic. 📊
    """,
    'openapi_tags': [
        {
            'name': 'User',
            'description': 'Operations with user. Loads user data automatically through X-API-KEY header'
        },
        {
            'name': 'Post',
            'description': 'Operations with post. Loads article data automatically through X-API-KEY header'
        },
        {
            'name': 'Signup',
            'description': 'Registers user, and generate API key for api access'
        }
    ]
}

