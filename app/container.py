from dependency_injector import providers, containers

from .databases import Gateways
from .core.v1 import V1


class Container(containers.DeclarativeContainer):
    config = providers.Configuration()
    gateways = providers.Container(
        Gateways,
        config=config.gateways
    )

    v1 = providers.Container(
        V1,
        gateways=gateways
    )
