from dependency_injector import providers, containers
from .models import user as _user
from .models import post as _post
from .models import like as _like
from .models import dislike as _dislike
from .models import request as _request


class V1Models(containers.DeclarativeContainer):
    gateways = providers.DependenciesContainer()

    sf = gateways.postgres.provided.session

    _user_repo = providers.Factory(
        _user.UserRepository,
        session_factory=sf
    )
    _post_repo = providers.Factory(
        _post.PostRepository,
        session_factory=sf
    )
    _like_repo = providers.Factory(
        _like.LikeRepository,
        session_factory=sf
    )
    _dislike_repo = providers.Factory(
        _dislike.DislikeRepository,
        session_factory=sf
    )

    _request_repo = providers.Factory(
        _request.RequestRepository,
        session_factory=sf
    )

    user = providers.Factory(
        _user.UserService,
        repository=_user_repo
    )

    post = providers.Factory(
        _post.PostService,
        repository=_post_repo
    )

    like = providers.Factory(
        _like.LikeService,
        repository=_like_repo
    )

    dislike = providers.Factory(
        _dislike.DislikeService,
        repository=_dislike_repo
    )

    request = providers.Factory(
        _request.RequestService,
        repository=_request_repo
    )


class V1(containers.DeclarativeContainer):
    config = providers.Configuration()
    gateways = providers.DependenciesContainer()

    models = providers.Container(
        V1Models,
        gateways=gateways

    )
