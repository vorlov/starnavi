import sqlalchemy as sqa
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship
from typing import Callable, AsyncContextManager, List, Union

from app.databases.pg import Base
from .mixins import IdMixin, TimeMixin
from . import User, Post


class Like(IdMixin, TimeMixin, Base):
    __tablename__ = 'like'

    user_id = sqa.Column(sqa.Integer, sqa.ForeignKey('user.id'))
    post_id = sqa.Column(sqa.Integer, sqa.ForeignKey('post.id'))

    user = relationship('User', back_populates='likes', lazy='subquery')
    post = relationship('Post', back_populates='likes', lazy='subquery')

class LikeRepository:
    def __init__(self, session_factory: Callable[..., AsyncContextManager[AsyncSession]]):
        self._session_factory = session_factory

    async def add(self, user_id: int, post_id: int) -> Like:
        async with self._session_factory() as session:
            query = sqa.insert(Like).values(
                user_id=user_id,
                post_id=post_id
            )
            result = await session.execute(query)
            await session.commit()

            return await self.get_by_id(result.inserted_primary_key[0])

    async def get_by_id(self, like_id: int) -> Like:
        async with self._session_factory() as session:
            query = sqa.select(Like).where(Like.id == like_id)
            result = await session.execute(query)
            return result.scalar()

    async def get_by_user(self, user_id: int) -> List[Like]:
        async with self._session_factory() as session:
            query = sqa.select(Like).where(User.id == user_id)
            result = await session.execute(query)
            return list(result.scalars())

    async def get_by_post(self, post_id: int) -> List[Like]:
        async with self._session_factory() as session:
            query = sqa.select(Like).where(Post.id == post_id)
            result = await session.execute(query)
            return list(result.scalars())

    async def get_by_user_post(self, user_id: int, post_id: int) -> Like:
        async with self._session_factory() as session:
            query = sqa.select(Like).where(sqa.and_(Post.id == post_id, Post.user_id == user_id))
            result = await session.execute(query)
            return result.scalar()

    async def delete(self, like_id: int) -> None:
        async with self._session_factory() as session:
            query = sqa.delete(Like).where(Like.id == like_id)
            await session.execute(query)
            await session.commit()


class LikeService:
    def __init__(self, repository: LikeRepository):
        self._repo = repository

    async def like(self, user_id: int, post_id: int) -> Union[Like, None]:
        like = await self._repo.get_by_user_post(user_id, post_id)
        if like:
            return await self._repo.delete(like.id)

        return await self._repo.add(user_id, post_id)

    async def remove_like(self, user_id: int, post_id: int) -> None:
        like_obj = await self._repo.get_by_user_post(user_id, post_id)
        await self._repo.delete(like_obj.id)

    async def is_liked(self, user_id: int, post_id: int) -> bool:
        like_obj = await self._repo.get_by_user_post(user_id, post_id)
        return bool(like_obj)

    async def get_by_id(self, like_id: int) -> Like:
        return await self._repo.get_by_id(like_id)

    async def get_all_user_likes(self, user_id: int) -> List[Like]:
        return await self._repo.get_by_user(user_id)

    async def get_all_post_likes(self, post: Post) -> List[Like]:
        return await self._repo.get_by_post(post.id)
