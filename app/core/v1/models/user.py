import secrets
import datetime
import sqlalchemy as sqa

from typing import Callable, AsyncContextManager, Tuple, List
from sqlalchemy.orm import relationship
from sqlalchemy.ext.asyncio import AsyncSession
from app.databases.pg import Base

from .mixins import IdMixin, TimeMixin


class User(IdMixin, TimeMixin, Base):
    __tablename__ = 'user'

    f_name = sqa.Column(sqa.Text, nullable=False)
    l_name = sqa.Column(sqa.Text, nullable=False)
    last_login = sqa.Column(sqa.DateTime(timezone=True))
    api_key = sqa.Column(sqa.Text, default=secrets.token_urlsafe)

    posts = relationship('Post', lazy='subquery', back_populates='user')
    requests = relationship('Request', lazy='subquery', back_populates='user')

    likes = relationship('Like', lazy='subquery', back_populates='user')
    dislikes = relationship('Dislike', lazy='subquery', back_populates='user')


class UserRepository:
    def __init__(self, session_factory: Callable[..., AsyncContextManager[AsyncSession]]):
        self._session_factory = session_factory

    async def add(self, first_name: str, last_name: str) -> User:
        async with self._session_factory() as session:
            query = sqa.insert(User).values(f_name=first_name, l_name=last_name)
            result = await session.execute(query)
            await session.commit()

            return await self.get_by_id(result.inserted_primary_key[0])

    async def get_by_id(self, user_id: int) -> User:
        async with self._session_factory() as session:
            query = sqa.select(User).where(User.id == user_id)
            result = await session.execute(query)
            return result.scalar()

    async def get_by_api_key(self, api_key: str) -> User:
        async with self._session_factory() as session:
            query = sqa.select(User).where(User.api_key == api_key)
            result = await session.execute(query)
            return result.scalar()

    async def update(self, user_id: int, **kw):
        async with self._session_factory() as session:
            query = sqa.update(User).where(User.id == user_id).values(**kw)
            await session.execute(query)
            await session.commit()

    async def aggregate_by(self, user_id: int, aggregate_by: str, date_from: str, date_to: str) -> List[Tuple]:
        async with self._session_factory() as session:
            query = f"""
            with requests_per_period as (
            select date_trunc('{aggregate_by}', request.created_at) as "period", count(*) as "requests"
            from request
                inner join "user" u on request.user_id = u.id
            where request.created_at between '{date_from}' and '{date_to}' and user_id = {user_id}
            group by 1
            ), dislikes_per_period as (
            select date_trunc('{aggregate_by}', dislike.created_at) as "period", count(*) as "dislikes"
            from dislike
                inner join "user" u on dislike.user_id = u.id
            where dislike.created_at between '{date_from}' and '{date_to}' and user_id = {user_id}
            group by 1
            ), likes_per_period as (
            select date_trunc('{aggregate_by}', "like".created_at) as "period", count(*) as "likes"
            from "like"
                inner join "user" u on "like".user_id = u.id
            where "like".created_at between '{date_from}' and '{date_to}' and user_id = {user_id}
            group by 1
            )
            select t.period, requests, likes, dislikes
            from generate_series(
                    timestamp '{date_from}',
                    timestamp '{date_to}',
                    interval '1 {aggregate_by}') as t(period)
            full outer join requests_per_period rph on rph.period = t.period
            full outer join dislikes_per_period dph on dph.period = t.period
            full outer join likes_per_period lph on lph.period = t.period
            """
            result = await session.execute(query)
            return list(result.all())


class UserService:
    def __init__(self, repository: UserRepository):
        self._repo: UserRepository = repository

    async def create_user(self, first_name: str, last_name: str) -> User:
        return await self._repo.add(first_name, last_name)

    async def get_user_by_id(self, user_id: int) -> User:
        return await self._repo.get_by_id(user_id)

    async def get_user_by_api_key(self, api_key: str) -> User:
        return await self._repo.get_by_api_key(api_key)

    async def login_user(self, user_id: int):
        await self._repo.update(user_id, last_login=datetime.datetime.now())

    async def aggregate_by(self, user_id: int, by: str, date_from: str, date_to: str) -> List[Tuple]:
        return await self._repo.aggregate_by(user_id, by, date_from, date_to)
