import enum
import json

import sqlalchemy as sqa
from typing import Callable, AsyncContextManager, Optional, Dict
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship

from app.databases.pg import Base
from .mixins import IdMixin, TimeMixin


class Request(IdMixin, TimeMixin, Base):
    __tablename__ = 'request'

    class RequestMethods(str, enum.Enum):
        get = 'GET'
        post = 'POST'
        put = 'PUT'
        delete = 'DELETE'

    method = sqa.Column(sqa.Enum(RequestMethods), nullable=False)
    url = sqa.Column(sqa.Text, nullable=False)
    data = sqa.Column(sqa.JSON, nullable=True)
    headers = sqa.Column(sqa.JSON, nullable=True)

    user_id = sqa.Column(sqa.Integer, sqa.ForeignKey('user.id'))
    user = relationship('User', back_populates='requests', lazy='subquery')


class RequestRepository:
    def __init__(self, session_factory: Callable[..., AsyncContextManager[AsyncSession]]):
        self._session_factory = session_factory

    async def add(self, user_id: int,
                  method: str,
                  url: str,
                  data: Optional[Dict] = None,
                  headers: Optional[Dict] = None) -> Request:
        async with self._session_factory() as session:
            query = sqa.insert(Request).values(
                method=method,
                url=url,
                data=data,
                headers=headers,
                user_id=user_id
            )
            result = await session.execute(query)
            await session.commit()

            return await self.get_by_id(result.inserted_primary_key[0])

    async def get_by_id(self, req_id: int) -> Request:
        async with self._session_factory() as session:
            query = sqa.select(Request).where(Request.id == req_id)
            result = await session.execute(query)
            return result.scalar()


class RequestService:
    def __init__(self, repository: RequestRepository):
        self._repo = repository

    async def create_request(self, user_id: int,
                             method: str,
                             url: str,
                             data: Optional[Dict] = None,
                             headers: Optional[Dict] = None) -> Request:
        return await self._repo.add(user_id, method, url, data, headers)
