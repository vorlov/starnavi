import sqlalchemy as sqa
from typing import Callable, AsyncContextManager, List, Optional, Tuple
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship

from app.databases.pg import Base
from .mixins import IdMixin, TimeMixin
from . import User


class Post(IdMixin, TimeMixin, Base):
    __tablename__ = 'post'

    title = sqa.Column(sqa.Text, nullable=False)
    description = sqa.Column(sqa.Text)
    text = sqa.Column(sqa.Text)

    user_id = sqa.Column(sqa.Integer, sqa.ForeignKey('user.id'))

    user = relationship('User', lazy='subquery', back_populates='posts')
    likes = relationship('Like', lazy='subquery', back_populates='post')
    dislikes = relationship('Dislike', lazy='subquery', back_populates='post')


class PostRepository:
    def __init__(self, session_factory: Callable[..., AsyncContextManager[AsyncSession]]):
        self._session_factory = session_factory

    async def add(self,
                  user_id: int,
                  title: str,
                  description: Optional[str] = None,
                  text: Optional[str] = None) -> Post:
        async with self._session_factory() as session:
            query = sqa.insert(Post).values(
                user_id=user_id,
                title=title,
                description=description,
                text=text
            )
            result = await session.execute(query)
            await session.commit()

            return await self.get_by_id(result.inserted_primary_key[0])

    async def get_by_id(self, post_id: int) -> Post:
        async with self._session_factory() as session:
            query = sqa.select(Post).where(Post.id == post_id)
            result = await session.execute(query)
            return result.scalar()

    async def get_by_user(self, user_id: int) -> List[Post]:
        async with self._session_factory() as session:
            query = sqa.select(Post).where(User.id == user_id)
            result = await session.execute(query)
            return list(result.scalars())

    async def get_all_posts(self) -> List[Post]:
        async with self._session_factory() as session:
            query = sqa.select(Post)
            result = await session.execute(query)
            return list(result.scalars())

    async def delete(self, post_id) -> None:
        async with self._session_factory() as session:
            query = sqa.delete(Post).where(Post.id == post_id)
            await session.execute(query)

    async def aggregate_by(self,
                           user_id: int,
                           post_id: int,
                           aggregate_by: str,
                           date_from: str,
                           date_to: str) -> List[Tuple]:
        async with self._session_factory() as session:
            query = f"""
            with dislikes_per_period as (
            select date_trunc('{aggregate_by}', dislike.created_at) as "period", count(*) as "dislikes"
            from dislike
                inner join "post" p on dislike.post_id = p.id
            where dislike.created_at between '{date_from}' and '{date_to}' and post_id = {post_id} and p.user_id = {user_id}
            group by 1
            ), likes_per_period as (
            select date_trunc('{aggregate_by}', "like".created_at) as "period", count(*) as "likes"
            from "like"
                inner join "post" p on "like".post_id = p.id
            where "like".created_at between '{date_from}' and '{date_to}' and post_id = {post_id} and p.user_id = {user_id}
            group by 1
            )
            select t.period, likes, dislikes
            from generate_series(
                    timestamp '{date_from}',
                    timestamp '{date_to}',
                    interval '1 {aggregate_by}') as t(period)
            full outer join dislikes_per_period dph on dph.period = t.period
            full outer join likes_per_period lph on lph.period = t.period
            """
            result = await session.execute(query)
            return list(result.all())


class PostService:
    def __init__(self, repository: PostRepository):
        self._repo = repository

    async def create_post(self,
                          user_id: int,
                          title: str,
                          description: Optional[str] = None,
                          text: Optional[str] = None) -> Post:
        return await self._repo.add(user_id, title, description, text)

    async def get_by_user(self, user_id: int) -> List[Post]:
        return await self._repo.get_by_user(user_id)

    async def get_by_id(self, post_id: int) -> Post:
        return await self._repo.get_by_id(post_id)

    async def get_all_posts(self) -> List[Post]:
        return await self._repo.get_all_posts()

    async def delete_post(self, post_id: int) -> None:
        return await self._repo.delete(post_id)

    async def aggregate_by(self, user_id: int, post_id: int, by: str, date_from: str, date_to: str) -> List[Tuple]:
        return await self._repo.aggregate_by(user_id, post_id, by, date_from, date_to)
