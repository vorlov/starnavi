import sqlalchemy as sqa
from typing import Callable, AsyncContextManager, List, Union
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship

from app.databases.pg import Base
from .mixins import IdMixin, TimeMixin
from . import User, Post


class Dislike(IdMixin, TimeMixin, Base):
    __tablename__ = 'dislike'

    user_id = sqa.Column(sqa.Integer, sqa.ForeignKey('user.id'))
    post_id = sqa.Column(sqa.Integer, sqa.ForeignKey('post.id'))

    user = relationship('User', back_populates='dislikes', lazy='subquery')
    post = relationship('Post', back_populates='dislikes', lazy='subquery')


class DislikeRepository:
    def __init__(self, session_factory: Callable[..., AsyncContextManager[AsyncSession]]):
        self._session_factory = session_factory

    async def add(self, user_id: int, post_id: int) -> Dislike:
        async with self._session_factory() as session:
            query = sqa.insert(Dislike).values(
                user_id=user_id,
                post_id=post_id
            )
            result = await session.execute(query)
            await session.commit()

            return await self.get_by_id(result.inserted_primary_key[0])

    async def get_by_id(self, dislike_id: int) -> Dislike:
        async with self._session_factory() as session:
            query = sqa.select(Dislike).where(Dislike.id == dislike_id)
            result = await session.execute(query)
            return result.scalar()

    async def get_by_user(self, user_id: int) -> List[Dislike]:
        async with self._session_factory() as session:
            query = sqa.select(Dislike).where(User.id == user_id)
            result = await session.execute(query)
            return list(result.scalars())

    async def get_by_post(self, post_id: int) -> List[Dislike]:
        async with self._session_factory() as session:
            query = sqa.select(Dislike).where(Post.id == post_id)
            result = await session.execute(query)
            return list(result.scalars())

    async def get_by_user_post(self, user_id: int, post_id: int) -> Dislike:
        async with self._session_factory() as session:
            query = sqa.select(Dislike).where(sqa.and_(Post.id == post_id, Post.user_id == user_id))
            result = await session.execute(query)
            return result.scalar()

    async def delete(self, dislike_id: int) -> None:
        async with self._session_factory() as session:
            query = sqa.delete(Dislike).where(Dislike.id == dislike_id)
            await session.execute(query)
            await session.commit()


class DislikeService:
    def __init__(self, repository: DislikeRepository):
        self._repo = repository

    async def dislike(self, user_id: int, post_id: int) -> Union[Dislike, None]:
        dislike = await self._repo.get_by_user_post(user_id, post_id)
        if dislike:
            return await self._repo.delete(dislike.id)

        return await self._repo.add(user_id, post_id)

    async def remove_dislike(self, user_id: int, post_id: int) -> None:
        dislike_obj = await self._repo.get_by_user_post(user_id, post_id)
        await self._repo.delete(dislike_obj.id)

    async def is_disliked(self, user_id: int, post_id: int) -> bool:
        dislike_obj = await self._repo.get_by_user_post(user_id, post_id)
        return bool(dislike_obj)

    async def get_by_id(self, dislike_id: int) -> Dislike:
        return await self._repo.get_by_id(dislike_id)

    async def get_all_user_dislikes(self, user_id: int) -> List[Dislike]:
        return await self._repo.get_by_user(user_id)

    async def get_all_post_dislikes(self, post: Post) -> List[Dislike]:
        return await self._repo.get_by_post(post.id)
