import sqlalchemy as sqa
from sqlalchemy.sql import func


class IdMixin(object):
    id = sqa.Column(sqa.Integer, primary_key=True, autoincrement=True)


class TimeMixin(object):
    created_at = sqa.Column(sqa.DateTime(timezone=True), default=func.now())
    updated_at = sqa.Column(sqa.DateTime(timezone=True), onupdate=func.now())
