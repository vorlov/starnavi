from pydantic import BaseModel
from typing import Union, Optional
import datetime


class CreatePost(BaseModel):
    title: str
    description: Optional[str]
    text: Optional[str]


class GetPost(BaseModel):
    id: int
    created: datetime.datetime
    title: str
    description: Union[str, None]
    text: Union[str, None]


class PostAnalytics(BaseModel):
    likes: int
    dislikes: int
