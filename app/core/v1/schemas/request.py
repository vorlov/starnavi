from pydantic import BaseModel
from typing import Optional, Dict


class GetRequest(BaseModel):
    id: int
    method: str
    url: str
    data: Optional[Dict]
    headers: Optional[Dict]
