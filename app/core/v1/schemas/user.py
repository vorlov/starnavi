import datetime
from pydantic import BaseModel, Field
from typing import Union, Dict


class CreateUser(BaseModel):
    first_name: str
    last_name: str


class GetUser(BaseModel):
    created: datetime.datetime
    first_name: str
    last_name: str
    last_login: Union[datetime.datetime, None]
    api_key: str


class UserAnalytics(BaseModel):
    requests: int
    likes: int
    dislikes: int
