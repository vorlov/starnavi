from dependency_injector import containers, providers
from .pg import PgDatabase


class Gateways(containers.DeclarativeContainer):
    config = providers.Configuration()
    postgres = providers.Singleton(
        PgDatabase,
        url=config.postgres.url
    )
