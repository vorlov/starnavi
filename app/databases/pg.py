from typing import Callable, AsyncContextManager
from dictalchemy import make_class_dictable
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from contextlib import asynccontextmanager


class PgDatabase:
    def __init__(self, url: str):
        self.uri = 'postgresql+asyncpg://' + url
        self._engine = create_async_engine(self.uri)
        self._session_factory = sessionmaker(autocommit=False, autoflush=False, bind=self._engine, class_=AsyncSession)

    @asynccontextmanager
    async def session(self) -> Callable[..., AsyncContextManager[Session]]:
        session = self._session_factory()

        try:
            yield session
        except Exception as e:
            await session.rollback()
            raise e
        finally:
            await session.close()


Base = declarative_base()
make_class_dictable(Base)
