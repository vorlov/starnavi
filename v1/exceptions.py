from fastapi import Request
from fastapi.responses import JSONResponse


class ApiKeyIsNotProvided(Exception): ...


class ApiKeyIsInvalid(Exception): ...


async def api_key_is_not_provided_handler(request: Request, exc: ApiKeyIsNotProvided):
    return JSONResponse(
        status_code=400,
        content={
            'error': {
                'message': 'X-API-KEY header is not provided'
            }
        }
    )


async def api_key_is_invalid_handler(request: Request, exc: ApiKeyIsInvalid):
    return JSONResponse(
        status_code=401,
        content={
            'error': {
                'message': 'API key is invalid'
            }
        }
    )
