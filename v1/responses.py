API_KEY_400 = {
    400: {
        'description': 'This response returns when X-API-KEY header is not provided',
        'content': {
            'application/json': {
                'example': {
                    'error': {
                        'message': 'X-API-KEY header is not provided'
                    }
                }
            }

        }
    }
}

API_KEY_401 = {
    401: {
        'description': 'This response returns when X-API-KEY is invalid',
        'content': {
            'application/json': {
                'example': {
                    'error': {
                        'message': 'API key is invalid'
                    }
                }
            }
        }
    }
}

NOT_FOUND_404 = {
    404: {
        'description': 'Not found',
        'content': {
            'application/json': {
                'example': {
                    'detail': {'message': 'Not Found'}
                }
            }
        }
    }
}
