from fastapi import FastAPI, APIRouter, Depends

from .endpoints import signup_r, post_r, user_r
from .exceptions import *
from .dependencies import *


def init_v1_router(app: FastAPI):
    v1_router = APIRouter(prefix='/v1', dependencies=[Depends(push_req_to_database)])

    v1_router.include_router(signup_r)
    v1_router.include_router(post_r)
    v1_router.include_router(user_r)

    app.include_router(v1_router)

    app.add_exception_handler(ApiKeyIsInvalid, api_key_is_invalid_handler)
    app.add_exception_handler(ApiKeyIsNotProvided, api_key_is_not_provided_handler)

