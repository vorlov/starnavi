from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from dependency_injector.wiring import inject, Provide

from app.core.v1.models.user import UserService
from app.core.v1.schemas.user import GetUser, CreateUser
from app.container import Container

signup_r = APIRouter()


@signup_r.post('/signup',
               description='Registering user with api key generation. Generated key receives.',
               tags=['Signup'],
               response_model=GetUser)
@inject
async def register_user(
        user_service: UserService = Depends(Provide[Container.v1.models.user]),
        *, user: CreateUser,
):
    user = await user_service.create_user(first_name=user.first_name, last_name=user.last_name)

    return JSONResponse(status_code=200, content={
        'created': str(user.created_at),
        'first_name': user.f_name,
        'last_name': user.l_name,
        'last_login': user.last_login if not user.last_login else str(user.last_login),
        'api_key': user.api_key
    })
