import datetime
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import JSONResponse
from typing import Dict, List, Optional

from v1.dependencies import api_key_security_return_user
from v1.responses import API_KEY_400, API_KEY_401, NOT_FOUND_404
from app.core.v1.models import User, UserService
from app.core.v1.schemas import user, post, request
from app.container import Container
from dependency_injector.wiring import inject, Provide

user_r = APIRouter(tags=['User'])


@user_r.get('/user',
            description='Returns user info',
            response_model=user.GetUser,
            responses={
                **API_KEY_400,
                **API_KEY_401,
            })
async def get_user_info(
        user: User = Depends(api_key_security_return_user)
):
    return JSONResponse(
        status_code=200,
        content={
            'created': str(user.created_at),
            'first_name': user.f_name,
            'last_name': user.l_name,
            'last_login': user.last_login if not user.last_login else str(user.last_login),
            'api_key': user.api_key
        }
    )


@user_r.get('/user/analytics',
            description='Returns analytics about user actions aggregated by interval. '
                        'If range is not provided, returns all count of requests, likes, dislikes',
            responses={
                **API_KEY_400,
                **API_KEY_401
            },
            response_model=Dict[str, user.UserAnalytics])
@inject
async def get_analytics(
        user: User = Depends(api_key_security_return_user), *,
        user_service: UserService = Depends(Provide[Container.v1.models.user]),
        aggregate_by: Optional[str] = 'day', date_from: Optional[str] = None, date_to: Optional[str] = None
):
    if not all([aggregate_by, date_from, date_to]):
        return JSONResponse(
            status_code=200,
            content={
                str(datetime.datetime.now()): {
                    'likes': len(user.likes),
                    'dislikes': len(user.dislikes),
                    'requests': len(user.requests)
                }
            }
        )
    else:
        result = await user_service.aggregate_by(user.id, aggregate_by, date_from, date_to)
        result = list(map(lambda row: (str(row[0]), row[1] or 0, row[2] or 0, row[3] or 0), result))
        response = {}

        for date, requests, likes, dislikes in result:
            response[str(date)] = {
                'requests': requests,
                'likes': likes,
                'dislikes': dislikes
            }
        return JSONResponse(
            status_code=200,
            content=response
        )


@user_r.get('/user/requests',
            description='Returns all user requests',
            response_model=List[request.GetRequest],
            responses={
                **API_KEY_400,
                **API_KEY_401,
                **NOT_FOUND_404
            })
async def get_user_requests(
        user: User = Depends(api_key_security_return_user)
):
    if not user.requests:
        raise HTTPException(status_code=404)

    response_data = []
    for req in user.requests:
        dict_: Dict = req.asdict()

        for key in ['updated_at', 'user_id']:
            dict_.pop(key)
        dict_['created'] = str(dict_.pop('created_at'))

        response_data.append(dict_)
    return JSONResponse(
        status_code=200,
        content=response_data
    )


@user_r.get('/user/posts',
            description='Returns all articles written by the user',
            response_model=List[post.GetPost],
            responses={
                **API_KEY_400,
                **API_KEY_401,
                **NOT_FOUND_404
            })
async def get_user_posts(
        user: User = Depends(api_key_security_return_user)
):
    if not user.posts:
        raise HTTPException(status_code=404)

    response_data = []
    for post in user.posts:
        dict_: Dict = post.asdict()

        for key in ['updated_at', 'user_id']:
            dict_.pop(key)

        dict_['created'] = str(dict_.pop('created_at'))
        response_data.append(dict_)
    return JSONResponse(
        status_code=200,
        content=response_data
    )


@user_r.get('/user/likes',
            description='Returns all articles liked by user',
            response_model=List[post.GetPost],
            responses={
                **API_KEY_400,
                **API_KEY_401,
                **NOT_FOUND_404
            })
async def get_user_likes(
        user: User = Depends(api_key_security_return_user)
):
    if not user.likes:
        raise HTTPException(status_code=404)

    response_data = []

    for like in user.likes:
        dict_: Dict = like.post.asdict()
        for key in ['updated_at', 'user_id']:
            dict_.pop(key)

        dict_['created'] = str(dict_.pop('created_at'))

        response_data.append(dict_)
    return JSONResponse(
        status_code=200,
        content=response_data
    )


@user_r.get('/user/dislikes',
            description='Returns all articles disliked by user',
            response_model=List[post.GetPost],
            responses={
                **API_KEY_400,
                **API_KEY_401,
                **NOT_FOUND_404
            })
async def get_user_dislikes(
        user: User = Depends(api_key_security_return_user)
):
    if not user.dislikes:
        raise HTTPException(status_code=404)

    response_data = []

    for dislike in user.dislikes:
        dict_: Dict = dislike.post.asdict()
        for key in ['updated_at', 'user_id']:
            dict_.pop(key)

        dict_['created'] = str(dict_.pop('created_at'))

        response_data.append(dict_)
    return JSONResponse(
        status_code=200,
        content=response_data
    )
