import datetime
from fastapi import APIRouter, Depends, Path
from fastapi.responses import JSONResponse
from dependency_injector.wiring import inject, Provide
from typing import List, Dict, Optional

from v1.dependencies import api_key_security_return_user, api_key_security_return_key, check_if_post_exists
from app.core.v1.models import User, PostService, DislikeService, LikeService
from v1.responses import API_KEY_400, API_KEY_401, NOT_FOUND_404
from app.core.v1.schemas.post import GetPost, CreatePost, PostAnalytics
from app.container import Container

post_r = APIRouter(tags=['Post'])


@post_r.get('/post',
            description='Returns all articles created by all users',
            response_model=List[GetPost],
            responses=NOT_FOUND_404)
@inject
async def get_all_posts(
        post_service: PostService = Depends(Provide[Container.v1.models.post])
):
    posts = await post_service.get_all_posts()
    if not posts:
        return 404

    response_data = []
    for post in posts:
        dict_: Dict = post.asdict()

        for key in ['updated_at', 'user_id']:
            dict_.pop(key)

        dict_['created'] = str(dict_.pop('created_at'))
        response_data.append(dict_)

    return JSONResponse(
        status_code=200,
        content=response_data
    )


@post_r.post('/post',
             description='Creates post',
             response_model=GetPost,
             responses={**API_KEY_400, **API_KEY_401})
@inject
async def create_post(
        post_service: PostService = Depends(Provide[Container.v1.models.post]),
        user: User = Depends(api_key_security_return_user), *, post: CreatePost
):
    post = await post_service.create_post(user.id,
                                          title=post.title,
                                          description=post.description,
                                          text=post.text)
    return JSONResponse(
        status_code=201,
        content={
            'id': post.id,
            'created': str(post.created_at),
            'title': post.title,
            'description': post.description,
            'text': post.text,
        }
    )


@post_r.get('/post/{post_id}',
            description='Returns post details',
            response_model=GetPost,
            responses={
                **API_KEY_400,
                **API_KEY_401,
                **NOT_FOUND_404
            },
            dependencies=[
                Depends(api_key_security_return_key),
                Depends(check_if_post_exists)
            ])
@inject
async def get_post_details(
        post_service: PostService = Depends(Provide[Container.v1.models.post]),
        post_id: int = Path(...)
):
    post = await post_service.get_by_id(post_id)
    dict_ = post.asdict()

    for key in ['updated_at', 'user_id']:
        dict_.pop(key)
    dict_['created'] = str(dict_.pop('created_at'))

    return JSONResponse(
        status_code=200,
        content=dict_
    )


@post_r.get('/post/{post_id}/analytics',
            description='Returns analytics about user post aggregated by interval. '
                        'If range is not provided, returns all count of likes, dislikes. '
                        'Only user who wrote post can see analytics',
            responses={
                **API_KEY_400,
                **API_KEY_401,
                **NOT_FOUND_404
            },
            response_model=Dict[str, PostAnalytics],
            dependencies=[
                Depends(api_key_security_return_key),
                Depends(check_if_post_exists)
            ])
@inject
async def get_post_analytics(
        user: User = Depends(api_key_security_return_user),
        post_service: PostService = Depends(Provide[Container.v1.models.post]),
        post_id: int = Path(...),
        aggregate_by: Optional[str] = 'day', date_from: Optional[str] = None, date_to: Optional[str] = None
):
    if not all([aggregate_by, date_from, date_to]):
        return JSONResponse(
            status_code=200,
            content={
                str(datetime.datetime.now()): {
                    'likes': len(user.likes),
                    'dislikes': len(user.dislikes),
                    'requests': len(user.requests)
                }
            }
        )
    else:
        result = await post_service.aggregate_by(user.id, post_id, aggregate_by, date_from, date_to)
        result = list(map(lambda row: (str(row[0]), row[1] or 0, row[2] or 0), result))
        response = {}

        for date, likes, dislikes in result:
            response[str(date)] = {
                'likes': likes,
                'dislikes': dislikes
            }
        return JSONResponse(
            status_code=200,
            content=response
        )


@post_r.post('/post/{post_id}/dislike',
             description='Dislikes post. If post was disliked than api removes dislike',
             responses={
                 **API_KEY_400,
                 **API_KEY_401,
                 **NOT_FOUND_404
             },
             dependencies=[
                 Depends(check_if_post_exists)
             ])
@inject
async def dislike_post(
        dislike_service: DislikeService = Depends(Provide[Container.v1.models.dislike]),
        like_service: LikeService = Depends(Provide[Container.v1.models.like]),
        user: User = Depends(api_key_security_return_user),
        post_id: int = Path(...)
):
    is_liked = await like_service.is_liked(user.id, post_id)

    if is_liked:
        await like_service.remove_like(user.id, post_id)

    await dislike_service.dislike(user.id, post_id)

    return 200


@post_r.post('/post/{post_id}/like',
             description='Likes post. If post was liked than api removes like',
             responses={
                 **API_KEY_400,
                 **API_KEY_401,
                 **NOT_FOUND_404
             },
             dependencies=[
                 Depends(check_if_post_exists)
             ])
@inject
async def like_post(
        like_service: LikeService = Depends(Provide[Container.v1.models.like]),
        dislike_service: DislikeService = Depends(Provide[Container.v1.models.dislike]),
        user: User = Depends(api_key_security_return_user),
        post_id: int = Path(...)
):
    is_disliked = await dislike_service.is_disliked(user.id, post_id)

    if is_disliked:
        await dislike_service.remove_dislike(user.id, post_id)

    await like_service.like(user.id, post_id)
    return 201
