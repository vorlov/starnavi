from json import JSONDecodeError

from fastapi.security import APIKeyHeader
from fastapi import Security, Depends, Path, HTTPException, Request

from dependency_injector.wiring import inject, Provide

from app.core.v1.models import PostService, Request as DbRequest
from app.core.v1.models.user import UserService, User
from app.container import Container
from .exceptions import ApiKeyIsInvalid, ApiKeyIsNotProvided

api_key = APIKeyHeader(name='X-API-Key', auto_error=False)


@inject
async def api_key_security_return_key(
        header_param: str = Security(api_key), *,
        user_service: UserService = Depends(Provide[Container.v1.models.user])
) -> str:
    if not header_param:
        raise ApiKeyIsNotProvided

    user = await user_service.get_user_by_api_key(header_param)

    if not user:
        raise ApiKeyIsInvalid
    return header_param


@inject
async def api_key_security_return_user(
        header_param: str = Security(api_key), *,
        user_service: UserService = Depends(Provide[Container.v1.models.user])
) -> User:
    if not header_param:
        raise ApiKeyIsNotProvided

    user = await user_service.get_user_by_api_key(header_param)

    if not user:
        raise ApiKeyIsInvalid
    return user


@inject
async def check_if_post_exists(
        post_id: int = Path(...),
        post_service: PostService = Depends(Provide[Container.v1.models.post])
):
    post = await post_service.get_by_id(post_id)
    if not post:
        raise HTTPException(status_code=404)


@inject
async def push_req_to_database(request: Request,
                               user_service=Depends(Provide[Container.v1.models.user]),
                               request_service=Depends(Provide[Container.v1.models.request])):
    if request.headers.get('X-API-KEY'):
        user = await user_service.get_user_by_api_key(request.headers['X-API-KEY'])
        if user and request.method in [item.value for item in DbRequest.RequestMethods]:
            try:
                post_data = await request.json()
            except JSONDecodeError:
                post_data = {}

            await request_service.create_request(user.id,
                                                 request.method.lower(),
                                                 request.url.path,
                                                 post_data,
                                                 dict(request.headers))
            await user_service.login_user(user.id)
